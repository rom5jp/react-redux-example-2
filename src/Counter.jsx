import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { inc, dec, stepChanged } from './counterActions'

// class Counter extends Component {
//   render() { return(
//     <div>
//       <h1>{this.props.counter.number}</h1>
//       <input onChange={this.props.stepChanged} value={this.props.counter.step} type='number' />
//       <button onClick={this.props.dec}>-</button>
//       <button onClick={this.props.inc}>+</button>
//     </div>
//   )}
// }

const Counter = props =>
  <div>
    <h1>{props.counter.number}</h1>
    <input onChange={props.stepChanged} value={props.counter.step} type='number' />
    <button onClick={props.dec}>-</button>
    <button onClick={props.inc}>+</button>
  </div>

const mapStateToProps = 
  state => ({ counter: state.counter })

const mapDispatchToProps = 
  dispatch => bindActionCreators({ inc, dec, stepChanged }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Counter)